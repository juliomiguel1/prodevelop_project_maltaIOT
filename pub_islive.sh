#!/bin/sh

varNpStat=$(mpstat -u  | tail -1)
IFS=' ' read -r -a arraynp <<< $varNpStat
varVmStat=$(vmstat | tail -1)
IFS=' ' read -r -a arraymem <<< $varVmStat
namehost=$(hostname)
vardiskIoT=$(df -P --total | tail -1)
IFS=' ' read -r -a arraydisk <<< $vardiskIoT
ipIoT=$(ip route get 1 | awk '{print $NF;exit}')
cpuNodeRed=$( ps aux | grep node-red | tail -2 | head -n 1)
IFS=' ' read -r -a arrayNodeRed <<< $cpuNodeRed	
timeSec=$(date +%s)
varFormatJson='{"name":"'$namehost'","ip":"'$ipIoT'","cpu_iot":"'${arraynp[2]}'","mem_free_iot":"'${arraymem[3]}'","disk_free_iot":"'${arraydisk[3]}'","cpu_nodeRed":"'${arrayNodeRed[2]}'","time":"'$timeSec'"}'
mosquitto_pub -h 130.0.2.149 -m `echo $varFormatJson` -t islive

exit 0

