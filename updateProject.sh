#!/bin/sh

cd /home/root/.node-red/projects/prodevelop_project_maltaIOT
git reset --hard HEAD
git pull origin master
git merge master origin
ipIoT=$(ip route get 1 | awk '{print $NF;exit}')

sed -i 's/iot1/'$ipIoT'-id/g' "flow.json"
curl -X POST http://$ipIoT:1880/flows -H "Content-Type: application/json" --data "@flow.json"
cd /home/root/.node-red/projects/first_project_remote
verCommit=$(git log --all -M -C --numstat --date=raw | head -n 5 | tail -1 | sed 's/^[[:space:]]*//')
timeCommit=$(git log --all -M -C --numstat --date=raw | head -n 5 | tail -3 | head -n 1 | sed  's/Date:   //g' | sed 's/ +0000//g' | sed 's/ +0200//g')
namehost=$(hostname)
varFinalString='{"version":"'$verCommit'","time":"'$timeCommit'","name":"'$namehost'","ip":"'$ipIoT'"}'
echo $varFinalString
mosquitto_pub -h 130.0.2.149 -m `echo $varFinalString` -t updateProject

exit 0
